﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Api
{
    /// <summary>
    /// Public model for adding a favorite city.
    /// </summary>
    public class AddFavoriteCity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddFavoriteCity"/> class.
        /// </summary>
        /// <param name="cityName">Name of the city.</param>
        /// <param name="country">The country.</param>
        /// <exception cref="System.ArgumentNullException">cityName</exception>
        /// <exception cref="System.ArgumentNullException">country</exception>
        public AddFavoriteCity(string cityName, string country)
        {
            this.CityName = cityName ?? throw new ArgumentNullException(nameof(cityName));
            this.Country = country ?? throw new ArgumentNullException(nameof(country));
        }

        /// <summary>
        /// The name of the city.
        /// </summary>
        /// <example>San Luis Obispo</example>
        [Required]
        [StringLength(90, MinimumLength = 1)]
        public string CityName { get; }

        /// <summary>
        /// The 2 letter code of the country.
        /// </summary>
        /// <example>US</example>
        [Required]
        [StringLength(2)]
        public string Country { get; }
    }
}
