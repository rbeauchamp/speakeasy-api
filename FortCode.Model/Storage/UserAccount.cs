﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Storage
{
    /// <summary>
    /// The user account storage model.
    /// </summary>
    public class UserAccount : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserAccount" /> class.
        /// </summary>
        /// <param name="id">The primary key.</param>
        /// <param name="name">The user's full name.</param>
        /// <param name="email">The user's email address.</param>
        /// <param name="password">The user's cleartext password.</param>
        /// <exception cref="System.ArgumentNullException">name</exception>
        /// <exception cref="System.ArgumentNullException">email</exception>
        /// <exception cref="System.ArgumentNullException">password</exception>
        public UserAccount(int? id, string name, string email, string password) : base(id)
        {
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.Email = email ?? throw new ArgumentNullException(nameof(email));
            this.Password = password ?? throw new ArgumentNullException(nameof(password));
        }

        /// <summary>
        /// The user's full name.
        /// </summary>
        [Required]
        [StringLength(70, MinimumLength = 1)]
        public string Name { get; }

        /// <summary>
        /// The user's email address.
        /// </summary>
        [Required]
        [EmailAddress]
        [StringLength(320)]
        public string Email { get; }

        /// <summary>
        /// The user's cleartext password.
        /// Strength requirements:
        /// 1. At least one lower case letter,
        /// 2. At least one upper case letter,
        /// 3. At least special character,
        /// 4. At least one number
        /// 5. Between 8-99 characters length
        /// </summary>
        [Required]
        [StringLength(76)]
        public string Password { get; }

        /// <summary>
        /// The user's favorite cities.
        /// </summary>
        public ICollection<FavoriteCity> FavoriteCities { get; } = new HashSet<FavoriteCity>();
    }
}
