﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Storage
{
    /// <summary>
    /// The city storage model.
    /// </summary>
    public class City : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="City" /> class.
        /// </summary>
        /// <param name="id">The primary key.</param>
        /// <param name="cityName">Name of the city.</param>
        /// <param name="country">The country.</param>
        /// <exception cref="System.ArgumentNullException">cityName</exception>
        /// <exception cref="System.ArgumentNullException">country</exception>
        public City(int? id, string cityName, string country) : base(id)
        {
            this.CityName = cityName ?? throw new ArgumentNullException(nameof(cityName));
            this.Country = country ?? throw new ArgumentNullException(nameof(country));
        }

        /// <summary>
        /// Gets the name of the city.
        /// </summary>
        [Required]
        [StringLength(90, MinimumLength = 1)]
        public string CityName { get; }

        /// <summary>
        /// Gets the name of the country.
        /// </summary>
        [Required]
        [StringLength(60)]
        public string Country { get; }

        /// <summary>
        /// Gets the set of favorite cities.
        /// </summary>
        public ICollection<FavoriteCity> FavoriteCities { get; } = new HashSet<FavoriteCity>();
    }
}
