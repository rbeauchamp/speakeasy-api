﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Model.Api;
using FortCode.Model.Storage;
using FortCode.Repository;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Domain
{
    /// <inheritdoc />
    public class SpeakeasyDomain : ISpeakeasyDomain
    {
        private readonly IFortCodeRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpeakeasyDomain"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public SpeakeasyDomain(IFortCodeRepository repository)
        {
            this.repository = repository;
        }

        /// <inheritdoc />
        public async Task AddFavoriteCityAsync(int userId, AddFavoriteCity addFavoriteCity)
        {
            // Add the city if it doesn't exist
            var city = await this.repository.Cities.SingleOrDefaultAsync(city => city.CityName == addFavoriteCity.CityName && city.Country == addFavoriteCity.Country);
            if (city == null)
            {
                city = Map(addFavoriteCity);
                await this.repository.Cities.AddAsync(city);
            }

            if (!await this.FavoriteCityExistsAsync(userId, city))
            {
                var user = await this.repository.UserAccounts.FindAsync(userId);

                await this.repository.FavoriteCities.AddAsync(new FavoriteCity(default) { City = city, User = user });

                await this.repository.SaveChangesAsync();
            }
        }

        /// <inheritdoc />
        public async Task<List<FavoriteCityData>> GetFavoriteCitiesAsync(int userId)
        {
            return await this.repository.FavoriteCities
                .Include(favoriteCity => favoriteCity.City)
                .Where(favoriteCity => favoriteCity.UserId == userId)
                .Select(favoriteCity => new FavoriteCityData(favoriteCity.Id!.Value, favoriteCity.City!.CityName, favoriteCity.City!.Country))
                .ToListAsync();
        }

        /// <inheritdoc />
        public async Task DeleteFavoriteCityAsync(int userId, int favoriteCityId)
        {
            await this.repository.FavoriteCities
                .Where(favoriteCity => favoriteCity.Id == favoriteCityId && favoriteCity.UserId == userId)
                .DeleteFromQueryAsync();
        }

        /// <inheritdoc />
        public async Task AddSpeakeasyAsync(int userId, AddSpeakeasy addSpeakeasy)
        {
            var speakeasy = new Speakeasy(default, addSpeakeasy.FavoriteCityId, addSpeakeasy.BarName, addSpeakeasy.FavoriteDrinkName);
            await this.repository.Speakeasies.AddAsync(speakeasy);
            await this.repository.SaveChangesAsync();
        }

        /// <inheritdoc />
        public async Task<bool> IsValidCityIdForUserAsync(int userId, int favoriteCityId)
        {
            return await this.repository.FavoriteCities.AnyAsync(favoriteCity =>
                favoriteCity.Id == favoriteCityId && favoriteCity.UserId == userId);
        }

        /// <inheritdoc />
        public async Task<List<SpeakeasyData>> GetSpeakeasiesAsync(int favoriteCityId)
        {
            return await this.repository.Speakeasies
                .Include(speakeasy => speakeasy.FavoriteCity)
                .ThenInclude(favoriteCity => favoriteCity!.City)
                .Where(speakeasy => speakeasy.FavoriteCityId == favoriteCityId)
                .Select(speakeasy => new SpeakeasyData(speakeasy.Id!.Value, Map(speakeasy.FavoriteCity!), speakeasy.BarName, speakeasy.FavoriteDrinkName))
                .ToListAsync();
        }

        /// <inheritdoc />
        public async Task DeleteSpeakeasyAsync(int userId, int speakeasyId)
        {
            await this.repository.Speakeasies
                .Where(speakeasy => speakeasy.Id == speakeasyId && speakeasy.FavoriteCity!.UserId == userId)
                .DeleteFromQueryAsync();
        }

        /// <inheritdoc />
        public async Task<List<SharedSpeakeasyView>> GetSharedSpeakeasiesAsync(int userId)
        {
            var userSpeakeasies = await this.repository.Speakeasies
                .Include(speakeasy => speakeasy.FavoriteCity)
                .Where(speakeasy => speakeasy.FavoriteCity!.UserId == userId)
                .ToListAsync();

            var sharedSpeakeasies = new List<SharedSpeakeasyView>();

            foreach (var userSpeakeasy in userSpeakeasies)
            {
                var sharedSpeakeasyCount = await this.repository.Speakeasies
                    // Not the current user
                    .Where(sharedSpeakeasy => sharedSpeakeasy.FavoriteCity!.UserId != userId)
                    // In the same city
                    .Where(sharedSpeakeasy => sharedSpeakeasy.FavoriteCity!.CityId == userSpeakeasy.FavoriteCity!.CityId)
                    // With the same bar name
                    .Where(sharedSpeakeasy => sharedSpeakeasy.BarName == userSpeakeasy.BarName)
                    .CountAsync();

                if (sharedSpeakeasyCount > 0)
                {
                    sharedSpeakeasies.Add(new SharedSpeakeasyView(userSpeakeasy.BarName, sharedSpeakeasyCount));
                }
            }

            return sharedSpeakeasies;
        }

        private async Task<bool> FavoriteCityExistsAsync(int userId, City city)
        {
            if (city.Id == null)
            {
                return false;
            }

            return await this.repository.FavoriteCities.AnyAsync(favoriteCity =>
                favoriteCity.CityId == city.Id && favoriteCity.UserId == userId);
        }

        private static FavoriteCityData Map(FavoriteCity favoriteCity)
        {
            return new FavoriteCityData(favoriteCity.Id!.Value, favoriteCity.City!.CityName, favoriteCity.City!.Country);
        }

        private static City Map(AddFavoriteCity addFavoriteCity)
        {
            return new City(default, addFavoriteCity.CityName, addFavoriteCity.Country);
        }
    }
}
