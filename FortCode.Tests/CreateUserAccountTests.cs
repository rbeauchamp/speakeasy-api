﻿using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;
using FluentAssertions;
using FortCode.Model.Api;
using FortCode.Repository;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using BCryptNet = BCrypt.Net.BCrypt;

namespace FortCode.Tests
{
    [Collection("SetupCollection")]
    public class CreateUserAccountTests : TestsBase
    {
        public CreateUserAccountTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task it_creates_a_user_account()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            var createUserAccount = new CreateUserAccount("Martha Jones", "test@test.com", "M307q$cb5xp21#!S");

            // Act
            var response = await client.PostAsJsonAsync(Routes.UserAccounts, createUserAccount);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            using var serviceScope = this.Factory.Services.CreateScope();
            var repository = serviceScope.ServiceProvider.GetRequiredService<IFortCodeRepository>();
            repository.UserAccounts.Should().HaveCount(1);
            repository.UserAccounts.Single().Should().BeEquivalentTo(createUserAccount, options => options.Excluding(account => account.Password));
        }

        [Fact]
        public async Task it_ensures_user_account_emails_are_unique()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            const string duplicateEmail = "test@test.com";
            await client.PostAsJsonAsync(Routes.UserAccounts, new CreateUserAccount("Martha Jones", duplicateEmail, "M307q$cb5xp21#!S"));

            // Act
            var response = await client.PostAsJsonAsync(Routes.UserAccounts, new CreateUserAccount("Don Jones", duplicateEmail, "2WRIiH^q8ue"));

            // Assert
            response.IsSuccessStatusCode.Should().BeFalse();
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            (await response.Content.ReadAsStringAsync()).Should().Contain("The email address is already being used.");
        }

        [Fact]
        public async Task it_hashes_passwords()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            var clearTextPassword = "M307q$cb5xp21#!S";
            var createUserAccount = new CreateUserAccount("Martha Jones", "test@test.com", clearTextPassword);

            // Act
            await client.PostAsJsonAsync(Routes.UserAccounts, createUserAccount);

            // Assert
            using var serviceScope = this.Factory.Services.CreateScope();
            var repository = serviceScope.ServiceProvider.GetRequiredService<IFortCodeRepository>();
            var userAccount = repository.UserAccounts.Single();
            BCryptNet.EnhancedVerify(clearTextPassword, userAccount.Password).Should().BeTrue();
        }
    }
}
