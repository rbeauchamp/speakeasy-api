﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Api
{
    /// <summary>
    /// Public model for a speakeasy.
    /// </summary>
    public class SpeakeasyData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpeakeasyData"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="city">The city.</param>
        /// <param name="barName">Name of the bar.</param>
        /// <param name="favoriteDrinkName">Name of the favorite drink.</param>
        /// <exception cref="ArgumentNullException">city</exception>
        /// <exception cref="ArgumentNullException">barName</exception>
        /// <exception cref="ArgumentNullException">favoriteDrinkName</exception>
        public SpeakeasyData(int id, FavoriteCityData city, string barName, string favoriteDrinkName)
        {
            this.Id = id;
            this.City = city ?? throw new ArgumentNullException(nameof(city));
            this.BarName = barName ?? throw new ArgumentNullException(nameof(barName));
            this.FavoriteDrinkName = favoriteDrinkName ?? throw new ArgumentNullException(nameof(favoriteDrinkName));
        }

        /// <summary>
        /// The primary key
        /// </summary>
        /// <example>1</example>
        public int Id { get; }

        /// <summary>
        /// The speakeasy's city
        /// </summary>
        [Required]
        public FavoriteCityData City { get; }

        /// <summary>
        /// The name of the speakeasy.
        /// </summary>
        /// <example>The Chapter Room</example>
        [Required]
        [StringLength(256, MinimumLength = 1)]
        public string BarName { get; }

        /// <summary>
        /// The name of the favorite drink.
        /// </summary>
        /// <example>Martini</example>
        [Required]
        [StringLength(256, MinimumLength = 1)]
        public string FavoriteDrinkName { get; }
    }
}
