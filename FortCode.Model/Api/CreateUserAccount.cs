﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Api
{
    /// <summary>
    /// Public model for a creating a user account.
    /// </summary>
    public class CreateUserAccount
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateUserAccount"/> class.
        /// </summary>
        /// <param name="name">The user's full name.</param>
        /// <param name="email">The user's email address.</param>
        /// <param name="password">The user's cleartext password.</param>
        /// <exception cref="ArgumentNullException">name</exception>
        /// <exception cref="ArgumentNullException">email</exception>
        /// <exception cref="ArgumentNullException">password</exception>
        public CreateUserAccount(string name, string email, string password)
        {
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.Email = email ?? throw new ArgumentNullException(nameof(email));
            this.Password = password ?? throw new ArgumentNullException(nameof(password));
        }

        /// <summary>
        /// The user's full name.
        /// </summary>
        /// <example>Martha Jones</example>
        [Required]
        [StringLength(70, MinimumLength = 1)]
        public string Name { get; }

        /// <summary>
        /// The user's email address.
        /// </summary>
        /// <example>test@test.com</example>
        [Required]
        [EmailAddress]
        [StringLength(320)]
        public string Email { get; }

        /// <summary>
        /// The user's cleartext password.
        /// </summary>
        /// <example>dLF!oO10C$co$19q</example>
        [Required]
        [StringLength(50, MinimumLength = 8)]
        [RegularExpression("^.*(?=.{8,})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^&+=]).*$",
            ErrorMessage = "The password must be between 8-50 characters and contain a lower case letter, an upper case letter, one special character, and one number.")]
        public string Password { get; }
    }
}
