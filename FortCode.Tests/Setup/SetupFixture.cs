﻿using System;
using System.Threading.Tasks;
using FortCode.Repository;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace FortCode.Tests.Setup
{
    public class SetupFixture : IAsyncLifetime
    {
        /// <summary>
        /// Create a test database in the docker db container.
        /// </summary>
        public async Task InitializeAsync()
        {
            await InitializeDatabase();

            Environment.SetEnvironmentVariable(ConfigKeys.SigningKey, Guid.NewGuid().ToString());
        }

        private static async Task InitializeDatabase()
        {
            var dockerPortId = await DockerSqlDatabaseUtilities.EnsureDockerStartedAndGetContainerIdAndPortAsync();

            var sqlConnectionString = DockerSqlDatabaseUtilities.GetSqlConnectionStringToTestDb(Guid.NewGuid(), dockerPortId);

            Environment.SetEnvironmentVariable(ConfigKeys.DbConnectionString, sqlConnectionString);

            var options = new DbContextOptionsBuilder<FortCodeRepository>().UseSqlServer(sqlConnectionString).Options;

            await using var repository = new FortCodeRepository(options);

            await repository.Database.EnsureCreatedAsync();
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }
    }
}
