﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Api
{
    /// <summary>
    /// Public model for a user account.
    /// </summary>
    public class UserAccountData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateUserAccount"/> class.
        /// </summary>
        /// <param name="name">The user's full name.</param>
        /// <param name="email">The user's email address.</param>
        /// <exception cref="ArgumentNullException">name</exception>
        /// <exception cref="ArgumentNullException">email</exception>
        public UserAccountData(string name, string email)
        {
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.Email = email ?? throw new ArgumentNullException(nameof(email));
        }

        /// <summary>
        /// The user's full name.
        /// </summary>
        /// <example>Martha Jones</example>
        [Required]
        [StringLength(70, MinimumLength = 1)]
        public string Name { get; }

        /// <summary>
        /// The user's email address.
        /// </summary>
        /// <example>test@test.com</example>
        [Required]
        [EmailAddress]
        [StringLength(320)]
        public string Email { get; }
    }
}
