﻿using System.Threading;
using System.Threading.Tasks;
using FortCode.Model.Storage;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace FortCode.Repository
{
    /// <summary>
    /// The repository for the FortCodeDb.
    /// </summary>
    public interface IFortCodeRepository
    {
        /// <summary>
        ///  Gets the user accounts.
        /// </summary>
        DbSet<UserAccount> UserAccounts { get; }

        /// <summary>
        ///  Gets the cities.
        /// </summary>
        DbSet<City> Cities { get; }

        /// <summary>
        ///  Gets users' favorite cities.
        /// </summary>
        DbSet<FavoriteCity> FavoriteCities { get; }

        /// <summary>
        ///  Gets users' favorite cities.
        /// </summary>
        DbSet<Speakeasy> Speakeasies { get; }

        /// <summary>
        ///     Gets the database.
        /// </summary>
        DatabaseFacade Database { get; }

        /// <summary>
        ///     Saves all changes made in this context to the database.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>The number of state entries written to the database.</returns>
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
