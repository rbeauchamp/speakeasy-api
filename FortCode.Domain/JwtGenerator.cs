﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using FortCode.Model.Storage;
using Microsoft.IdentityModel.Tokens;

namespace FortCode.Domain
{
    /// <summary>
    /// Utility class for generating JWTs.
    /// </summary>
    public static class JwtGenerator
    {
        /// <summary>
        /// The JWT issuer.
        /// </summary>
        public const string Issuer = "auth.fortcode.com";

        /// <summary>
        /// Recipient for which the JWT is intended.
        /// </summary>
        public const string Audience = "client.fortcode.com";

        /// <summary>
        /// Generates a token for the given user.
        /// </summary>
        /// <param name="userAccount">The user account.</param>
        /// <param name="signingKey">The signing key.</param>
        public static string GenerateToken(UserAccount userAccount, string signingKey)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(signingKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = Issuer,
                Subject = new ClaimsIdentity(new[] { new Claim(ClaimTypes.NameIdentifier, userAccount.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Audience = Audience
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
