﻿namespace FortCode
{
    /// <summary>
    /// The configuration keys.
    /// </summary>
    public static class ConfigKeys
    {
        /// <summary>
        /// The database connection string
        /// </summary>
        public const string DbConnectionString = "ConnectionStrings:DbContext";

        /// <summary>
        /// The signing key
        /// </summary>
        public const string SigningKey = "AppSettings:SigningKey";
    }
}
