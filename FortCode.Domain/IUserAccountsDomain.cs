﻿using System.Threading.Tasks;
using FortCode.Model.Api;

namespace FortCode.Domain
{
    /// <summary>
    /// Business domain for creating user accounts.
    /// </summary>
    public interface IUserAccountsDomain
    {
        /// <summary>
        /// Creates a user account.
        /// </summary>
        /// <param name="createUserAccount">The user account data.</param>
        /// <returns>The newly created user account.</returns>
        Task CreateUserAccountAsync(CreateUserAccount createUserAccount);

        /// <summary>
        /// Determines if a user account already exists with the given email address.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        Task<bool> EmailExistsAsync(string emailAddress);

        /// <summary>
        /// Determines if a user exists with the given id.
        /// </summary>
        /// <param name="userId">The user id.</param>
        Task<bool> UserExistsAsync(int userId);

        /// <summary>
        /// Authenticates a user.
        /// </summary>
        /// <param name="authenticate">The authentication data.</param>
        /// <returns>The authentication result if successful, otherwise null.</returns>
        Task<AuthenticationResult?> AuthenticateAsync(Authenticate authenticate);
    }
}
