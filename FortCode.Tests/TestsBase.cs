﻿using FortCode.Repository;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace FortCode.Tests
{
    public abstract class TestsBase : IClassFixture<WebApplicationFactory<Startup>>
    {
        protected WebApplicationFactory<Startup> Factory { get; }

        protected TestsBase(WebApplicationFactory<Startup> factory)
        {
            this.Factory = factory;
            using var serviceScope = factory.Services.CreateScope();

            var repository = serviceScope.ServiceProvider.GetRequiredService<IFortCodeRepository>();

            // Clean out transactional data between tests
            repository.FavoriteCities.DeleteFromQuery();
            repository.UserAccounts.DeleteFromQuery();
            repository.Cities.DeleteFromQuery();
        }
    }
}
