# Speakeasy API
This assignment implements an API that enables users to manage a list of their favorite speakeasies and drinks. It leverages best practices around test driven development, application layering, inversion of control, validation, RESTful design, IO performance (via async/await) and nullability.

## Running the Automated Integration Tests
From command line

1. Navigate to the solution folder

2. Run `dotnet test`

All features have integration tests and code coverage is at 87%.

## Using the Swagger UI
From command line

1. Navigate to the solution folder

2. Run/update docker compose: `docker-compose up -d --build`

3. Once docker compose has been deployed successfully, you can access the Swagger UI at `http://localhost:8100/swagger`.