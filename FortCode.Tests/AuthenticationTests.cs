﻿using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;
using FluentAssertions;
using FortCode.Model.Api;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace FortCode.Tests
{
    [Collection("SetupCollection")]
    public class AuthenticationTests : TestsBase
    {
        public AuthenticationTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task it_enables_a_user_to_authenticate_with_an_email_and_password()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            var createUserAccount = new CreateUserAccount("Martha Jones", "test@test.com", "M307q$cb5xp21#!S");
            await client.PostAsJsonAsync(Routes.UserAccounts, createUserAccount);
            var authenticate = new Authenticate(createUserAccount.Email, createUserAccount.Password);

            // Act
            var response = await client.PostAsJsonAsync(Routes.Authenticate, authenticate);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task it_does_not_authenticate_a_missing_email()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            var authenticate = new Authenticate("fake@test.com", "7%8CC#wivL3Kd");

            // Act
            var response = await client.PostAsJsonAsync(Routes.Authenticate, authenticate);

            // Assert
            response.IsSuccessStatusCode.Should().BeFalse();
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            (await response.Content.ReadAsStringAsync()).Should().Contain("The email or password is incorrect.");
        }

        [Fact]
        public async Task it_does_not_authenticate_a_bogus_password()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            var createUserAccount = new CreateUserAccount("Martha Jones", "test@test.com", "M307q$cb5xp21#!S");
            await client.PostAsJsonAsync(Routes.UserAccounts, createUserAccount);
            var authenticate = new Authenticate(createUserAccount.Email, "bogus_99*Z$Ryj5sFPt");

            // Act
            var response = await client.PostAsJsonAsync(Routes.Authenticate, authenticate);

            // Assert
            response.IsSuccessStatusCode.Should().BeFalse();
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            (await response.Content.ReadAsStringAsync()).Should().Contain("The email or password is incorrect.");
        }
    }
}
