﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Api
{
    /// <summary>
    /// Public model for a user's favorite city.
    /// </summary>
    public class FavoriteCityData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FavoriteCityData" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cityName">Name of the city.</param>
        /// <param name="country">The country.</param>
        /// <exception cref="System.ArgumentNullException">cityName</exception>
        /// <exception cref="System.ArgumentNullException">country</exception>
        public FavoriteCityData(int id, string cityName, string country)
        {
            this.Id = id;
            this.CityName = cityName ?? throw new ArgumentNullException(nameof(cityName));
            this.Country = country ?? throw new ArgumentNullException(nameof(country));
        }

        /// <summary>
        /// The primary key
        /// </summary>
        /// <example>1</example>
        public int Id { get; }

        /// <summary>
        /// The name of the city.
        /// </summary>
        /// <example>San Luis Obispo</example>
        [Required]
        [StringLength(90, MinimumLength = 1)]
        public string CityName { get; }

        /// <summary>
        /// The 2 letter code of the country.
        /// </summary>
        /// <example>US</example>
        [Required]
        [StringLength(2)]
        public string Country { get; }
    }
}
