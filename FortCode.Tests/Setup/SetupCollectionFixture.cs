﻿using Xunit;

namespace FortCode.Tests.Setup
{
    /// <summary>
    ///     See https://xunit.github.io/docs/shared-context.html#collection-fixture.
    /// </summary>
    /// <seealso cref="ICollectionFixture{TFixture}" />
    [CollectionDefinition("SetupCollection")]
    public class SetupCollectionFixture : ICollectionFixture<SetupFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}
