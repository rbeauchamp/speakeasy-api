﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Api
{
    /// <summary>
    /// Public model for a shared speakeasy.
    /// </summary>
    public class SharedSpeakeasyView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SharedSpeakeasyView"/> class.
        /// </summary>
        /// <param name="barName">Name of the bar.</param>
        /// <param name="sharedCount">The shared count.</param>
        /// <exception cref="ArgumentNullException">barName</exception>
        public SharedSpeakeasyView(string barName, int sharedCount)
        {
            this.BarName = barName ?? throw new ArgumentNullException(nameof(barName));
            this.SharedCount = sharedCount;
        }

        /// <summary>
        /// The name of the speakeasy.
        /// </summary>
        /// <example>The Chapter Room</example>
        [Required]
        [StringLength(256, MinimumLength = 1)]
        public string BarName { get; }


        /// <summary>
        /// The number of other users that share it.
        /// </summary>
        /// <example>3</example>
        public int SharedCount { get; }
    }
}
