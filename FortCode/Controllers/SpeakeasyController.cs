﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;
using FortCode.Domain;
using FortCode.Model.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace FortCode.Controllers
{
    /// <summary>
    /// Manages speakeasies for a user.
    /// Implements <see cref="ControllerBase" />
    /// </summary>
    /// <seealso cref="ControllerBase" />
    [ApiController]
    public class SpeakeasyController : ControllerBase
    {
        private readonly ISpeakeasyDomain speakeasyDomain;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpeakeasyController"/> class.
        /// </summary>
        /// <param name="speakeasyDomain">The speakeasies domain.</param>
        public SpeakeasyController(ISpeakeasyDomain speakeasyDomain)
        {
            this.speakeasyDomain = speakeasyDomain;
        }

        /// <summary>
        /// Add a speakeasy in the authenticated user's favorite city.
        /// </summary>
        /// <param name="addSpeakeasy">The speakeasy data.</param>
        [Authorize]
        [HttpPost(Routes.Speakeasies)]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "The user is not authenticated.")]
        [SwaggerResponse(StatusCodes.Status201Created, "The speakeasy was successfully added.")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "The speakeasy data is invalid.", typeof(IDictionary<string, IList<string>>))]
        public async Task<IActionResult> Post([SwaggerRequestBody(Required = true)] AddSpeakeasy addSpeakeasy)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var userId = this.GetUserId();

            await this.speakeasyDomain.AddSpeakeasyAsync(userId, addSpeakeasy);

            return this.StatusCode(StatusCodes.Status201Created);
        }

        /// <summary>
        /// Retrieves a list of the currently authenticated user's speakeasies for the given favorite city.
        /// </summary>
        [Authorize]
        [HttpGet(Routes.Speakeasies)]
        [Produces(MediaTypeNames.Application.Json)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "The user is not authenticated.")]
        [SwaggerResponse(StatusCodes.Status200OK, "The user's speakeasies were retrieved.", typeof(List<SpeakeasyData>))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "The favorite city id is invalid.", typeof(IDictionary<string, IList<string>>))]
        public async Task<IActionResult> GetSpeakeasies([FromQuery] int favoriteCityId)
        {
            var userId = this.GetUserId();

            var isValidCityIdForUser = await this.speakeasyDomain.IsValidCityIdForUserAsync(userId, favoriteCityId);

            if (!isValidCityIdForUser)
            {
                this.ModelState.AddModelError(nameof(Authenticate), "The city id is invalid.");
                return this.BadRequest(this.ModelState);
            }

            var speakeasies = await this.speakeasyDomain.GetSpeakeasiesAsync(favoriteCityId);

            return this.Ok(speakeasies);
        }

        /// <summary>
        /// Add a favorite city for an authenticated user if not already added.
        /// </summary>
        /// <param name="addFavoriteCity">The favorite city data.</param>
        [Authorize]
        [HttpPost(Routes.FavoriteCities)]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "The user is not authenticated.")]
        [SwaggerResponse(StatusCodes.Status202Accepted, "The favorite city was successfully added.")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "The favorite city data is invalid.", typeof(IDictionary<string, IList<string>>))]
        public async Task<IActionResult> AddFavoriteCity([SwaggerRequestBody(Required = true)] AddFavoriteCity addFavoriteCity)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var userId = this.GetUserId();

            await this.speakeasyDomain.AddFavoriteCityAsync(userId, addFavoriteCity);

            return this.StatusCode(StatusCodes.Status202Accepted);
        }

        /// <summary>
        /// Retrieves a list of the currently authenticated user's favorite cities.
        /// </summary>
        [Authorize]
        [HttpGet(Routes.FavoriteCities)]
        [Produces(MediaTypeNames.Application.Json)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "The user is not authenticated.")]
        [SwaggerResponse(StatusCodes.Status200OK, "The user's favorite cities were retrieved.", typeof(List<FavoriteCityData>))]
        public async Task<IActionResult> GetFavoriteCities()
        {
            var userId = this.GetUserId();

            var favoriteCities = await this.speakeasyDomain.GetFavoriteCitiesAsync(userId);

            return this.Ok(favoriteCities);
        }

        /// <summary>
        /// Retrieves a list of the shared speakeasies for the given user.
        /// </summary>
        [Authorize]
        [HttpGet(Routes.SharedSpeakeasies)]
        [Produces(MediaTypeNames.Application.Json)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "The user is not authenticated.")]
        [SwaggerResponse(StatusCodes.Status200OK, "The shared speakeasies were retrieved.", typeof(List<SharedSpeakeasyView>))]
        public async Task<IActionResult> GetSharedSpeakeasies()
        {
            var userId = this.GetUserId();

            var sharedSpeakeasies = await this.speakeasyDomain.GetSharedSpeakeasiesAsync(userId);

            return this.Ok(sharedSpeakeasies);
        }

        /// <summary>
        /// Deletes a favorite city if it exists for the authenticated user.
        /// </summary>
        /// <param name="id">The id of the favorite city to delete.</param>
        [Authorize]
        [HttpDelete(Routes.FavoriteCityById)]
        [Produces(MediaTypeNames.Application.Json)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "The user is not authenticated.")]
        [SwaggerResponse(StatusCodes.Status200OK, "The user's favorite cities was deleted if it existed.", typeof(List<FavoriteCityData>))]
        public async Task<IActionResult> DeleteFavoriteCity([FromRoute] int id)
        {
            var userId = this.GetUserId();

            await this.speakeasyDomain.DeleteFavoriteCityAsync(userId, id);

            return this.Ok();
        }

        /// <summary>
        /// Deletes a speakeasy if it exists for the authenticated user.
        /// </summary>
        /// <param name="id">The id of the speakeasy to delete.</param>
        [Authorize]
        [HttpDelete(Routes.SpeakeasyById)]
        [Produces(MediaTypeNames.Application.Json)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "The user is not authenticated.")]
        [SwaggerResponse(StatusCodes.Status200OK, "The user's speakeasy was deleted if it existed.", typeof(List<FavoriteCityData>))]
        public async Task<IActionResult> DeleteSpeakeasy([FromRoute] int id)
        {
            var userId = this.GetUserId();

            await this.speakeasyDomain.DeleteSpeakeasyAsync(userId, id);

            return this.Ok();
        }

        private int GetUserId()
        {
            return int.Parse(this.User.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier).Value);
        }
    }
}
