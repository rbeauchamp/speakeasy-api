﻿using System;

namespace FortCode.Model.Storage
{
    /// <summary>
    /// Base class for all entities.
    /// </summary>
    public abstract class Entity : IEquatable<Entity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        /// <param name="id">The entity's primary key.</param>
        protected Entity(int? id)
        {
            this.Id = id;
        }

        /// <summary>
        /// The entity's primary key
        /// </summary>
        public int? Id { get; }

        /// <inheritdoc />
        public bool Equals(Entity? other)
        {
            if (other is null)
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.Id != null && other.Id != null && this.Id == other.Id;
        }

        /// <inheritdoc />
        public override bool Equals(object? obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return this.Equals((Entity)obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

#pragma warning disable CS1591
        public static bool operator ==(Entity? left, Entity? right)
        {
            return Equals(left, right);
        }

#pragma warning disable CS1591
        public static bool operator !=(Entity? left, Entity? right)
        {
            return !Equals(left, right);
        }
    }
}
