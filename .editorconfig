# To learn more about .editorconfig see https://aka.ms/editorconfigdocs
###############################
# Core EditorConfig Options   #
###############################
# All files
[*]
indent_style = space
dotnet_diagnostic.CA1047.severity=error
# Code files
[*.{cs,csx,vb,vbx}]
indent_size = 4
insert_final_newline = true
charset = utf-8-bom
###############################
# .NET Coding Conventions     #
###############################
[*.{cs,vb}]
# Organize usings
dotnet_sort_system_directives_first = true
# this. preferences
dotnet_style_qualification_for_field =true:error
dotnet_style_qualification_for_property =true:error
dotnet_style_qualification_for_method =true:error
dotnet_style_qualification_for_event =true:error
# Language keywords vs BCL types preferences
dotnet_style_predefined_type_for_locals_parameters_members =true:error
dotnet_style_predefined_type_for_member_access = true:silent
# Parentheses preferences
dotnet_style_parentheses_in_arithmetic_binary_operators = always_for_clarity:silent
dotnet_style_parentheses_in_relational_binary_operators = always_for_clarity:silent
dotnet_style_parentheses_in_other_binary_operators = always_for_clarity:silent
dotnet_style_parentheses_in_other_operators = never_if_unnecessary:silent
# Modifier preferences
dotnet_style_require_accessibility_modifiers = for_non_interface_members:silent
dotnet_style_readonly_field =true:error
# Expression-level preferences
dotnet_style_object_initializer =true:error
dotnet_style_collection_initializer =true:error
dotnet_style_explicit_tuple_names =true:error
dotnet_style_null_propagation =true:error
dotnet_style_coalesce_expression =true:error
dotnet_style_prefer_is_null_check_over_reference_equality_method =true:error
dotnet_style_prefer_inferred_tuple_names =true:error
dotnet_style_prefer_inferred_anonymous_type_member_names =true:error
dotnet_style_prefer_auto_properties =true:error
dotnet_style_prefer_conditional_expression_over_assignment = true:silent
dotnet_style_prefer_conditional_expression_over_return = true:silent
###############################
# Naming Conventions          #
###############################
# Style Definitions
dotnet_naming_style.pascal_case_style.capitalization             = pascal_case
# Use PascalCase for constant fields  
dotnet_naming_rule.constant_fields_should_be_pascal_case.severity = suggestion
dotnet_naming_rule.constant_fields_should_be_pascal_case.symbols  = constant_fields
dotnet_naming_rule.constant_fields_should_be_pascal_case.style    = pascal_case_style
dotnet_naming_symbols.constant_fields.applicable_kinds            = field
dotnet_naming_symbols.constant_fields.applicable_accessibilities  = *
dotnet_naming_symbols.constant_fields.required_modifiers          = const
dotnet_diagnostic.CA1303.severity=silent
dotnet_diagnostic.CA1307.severity=silent
dotnet_diagnostic.CA1308.severity=silent
dotnet_diagnostic.CA2101.severity=silent
dotnet_diagnostic.CA1000.severity=error
dotnet_diagnostic.CA1014.severity=silent
dotnet_diagnostic.CA1016.severity=silent
dotnet_diagnostic.CA1017.severity=silent
dotnet_diagnostic.CA1036.severity=error
dotnet_diagnostic.CA1041.severity=error
dotnet_diagnostic.CA1054.severity=error
dotnet_diagnostic.CA1061.severity=error
dotnet_diagnostic.CA1067.severity=error
dotnet_diagnostic.CA1068.severity=error
dotnet_diagnostic.CA1069.severity=error
dotnet_diagnostic.CA1501.severity=error
dotnet_diagnostic.CA1502.severity=error
dotnet_diagnostic.CA1505.severity=error
dotnet_diagnostic.CA1506.severity=error
dotnet_diagnostic.CA1508.severity=error
dotnet_diagnostic.CA1509.severity=error
dotnet_diagnostic.CA1700.severity=error
dotnet_diagnostic.CA1712.severity=error
dotnet_diagnostic.CA1713.severity=error
dotnet_diagnostic.CA1715.severity=error
dotnet_diagnostic.CA1716.severity=error
dotnet_diagnostic.CA1720.severity=error
dotnet_diagnostic.CA1721.severity=error
dotnet_diagnostic.CA1724.severity=error
dotnet_diagnostic.CA1725.severity=error
dotnet_diagnostic.CA1802.severity=error
dotnet_diagnostic.CA1805.severity=error
dotnet_diagnostic.CA1806.severity=silent
dotnet_diagnostic.CA1810.severity=error
dotnet_diagnostic.CA1813.severity=error
dotnet_diagnostic.CA1814.severity=error
dotnet_diagnostic.CA1815.severity=error
dotnet_diagnostic.CA1821.severity=error
dotnet_diagnostic.CA1822.severity=error
dotnet_diagnostic.CA1823.severity=error
dotnet_diagnostic.CA1826.severity=error
dotnet_diagnostic.CA1827.severity=error
dotnet_diagnostic.CA1828.severity=error
dotnet_diagnostic.CA1829.severity=error
dotnet_diagnostic.CA1830.severity=error
dotnet_diagnostic.CA1831.severity=error
dotnet_diagnostic.CA1832.severity=error
dotnet_diagnostic.CA1833.severity=error
dotnet_diagnostic.CA1834.severity=error
dotnet_diagnostic.CA1835.severity=error
dotnet_diagnostic.CA1836.severity=error
dotnet_diagnostic.CA1837.severity=error
dotnet_diagnostic.CA1838.severity=error
dotnet_diagnostic.IL3000.severity=error
dotnet_diagnostic.IL3001.severity=error
dotnet_style_prefer_simplified_boolean_expressions=true:error
dotnet_style_prefer_compound_assignment=true:error
###############################
# C# Coding Conventions       #
###############################
[*.cs]
# var preferences
csharp_style_var_for_built_in_types =true:error
csharp_style_var_when_type_is_apparent =true:error
csharp_style_var_elsewhere =true:error
# Expression-bodied members
csharp_style_expression_bodied_methods =false:error
csharp_style_expression_bodied_constructors =false:error
csharp_style_expression_bodied_operators =false:error
csharp_style_expression_bodied_properties =true:error
csharp_style_expression_bodied_indexers =true:error
csharp_style_expression_bodied_accessors =true:error
# Pattern matching preferences
csharp_style_pattern_matching_over_is_with_cast_check = true:suggestion
csharp_style_pattern_matching_over_as_with_null_check = true:suggestion
# Null-checking preferences
csharp_style_throw_expression =true:error
csharp_style_conditional_delegate_call =true:error
# Modifier preferences
csharp_preferred_modifier_order = public,private,protected,internal,static,extern,new,virtual,abstract,sealed,override,readonly,unsafe,volatile,async:suggestion
# Expression-level preferences
csharp_prefer_braces = true:silent
csharp_style_deconstructed_variable_declaration =true:error
csharp_prefer_simple_default_expression =true:error
csharp_style_pattern_local_over_anonymous_function =true:error
csharp_style_inlined_variable_declaration =true:error
###############################
# C# Formatting Rules         #
###############################
# New line preferences
csharp_new_line_before_open_brace = all
csharp_new_line_before_else = true
csharp_new_line_before_catch = true
csharp_new_line_before_finally = true
csharp_new_line_before_members_in_object_initializers = true
csharp_new_line_before_members_in_anonymous_types = true
csharp_new_line_between_query_expression_clauses = true
# Indentation preferences
csharp_indent_case_contents = true
csharp_indent_switch_labels = true
csharp_indent_labels = flush_left
# Space preferences
csharp_space_after_cast = false
csharp_space_after_keywords_in_control_flow_statements = true
csharp_space_between_method_call_parameter_list_parentheses = false
csharp_space_between_method_declaration_parameter_list_parentheses = false
csharp_space_between_parentheses = false
csharp_space_before_colon_in_inheritance_clause = true
csharp_space_after_colon_in_inheritance_clause = true
csharp_space_around_binary_operators = before_and_after
csharp_space_between_method_declaration_empty_parameter_list_parentheses = false
csharp_space_between_method_call_name_and_opening_parenthesis = false
csharp_space_between_method_call_empty_parameter_list_parentheses = false
# Wrapping preferences
csharp_preserve_single_line_statements = true
csharp_preserve_single_line_blocks = true

# CA2007: Consider calling ConfigureAwait on the awaited task
dotnet_diagnostic.CA2007.severity = none

# RCS1090: Add call to 'ConfigureAwait' (or vice versa).
dotnet_diagnostic.RCS1090.severity = none

# CA1062: Validate arguments of public methods
dotnet_diagnostic.CA1062.severity = none

# CA1002: Do not expose generic lists
dotnet_diagnostic.CA1002.severity = none

# CA2227: Collection properties should be read only
dotnet_diagnostic.CA2227.severity = none

# CA1707: Identifiers should not contain underscores
dotnet_diagnostic.CA1707.severity = none

# CA1031: Do not catch general exception types
dotnet_diagnostic.CA1031.severity = none

# Default severity for analyzer diagnostics with category 'Globalization'
dotnet_analyzer_diagnostic.category-Globalization.severity = none

# CA1305: Specify IFormatProvider
dotnet_diagnostic.CA1305.severity = none

dotnet_diagnostic.CA1812.severity = none
dotnet_diagnostic.xUnit2001.severity=error
dotnet_diagnostic.IDE0051.severity=error
dotnet_diagnostic.IDE0052.severity=error
dotnet_diagnostic.RCS1135.severity=error
dotnet_diagnostic.RCS1078i.severity=error
dotnet_diagnostic.RCS1157.severity=error
dotnet_diagnostic.RCS1158.severity=error
dotnet_diagnostic.RCS1160.severity=error
dotnet_diagnostic.RCS1169.severity=error
dotnet_diagnostic.RCS1170.severity=error
dotnet_diagnostic.RCS1187.severity=error
dotnet_diagnostic.RCS1224.severity=error
dotnet_diagnostic.RCS1225.severity=error
dotnet_diagnostic.CA1070.severity=error
dotnet_diagnostic.CA1001.severity=error
dotnet_diagnostic.RCS1006FadeOut.severity=error
dotnet_diagnostic.RCS1015FadeOut.severity=error
dotnet_diagnostic.RCS1005FadeOut.severity=error
dotnet_diagnostic.RCS1031FadeOut.severity=error
dotnet_diagnostic.RCS1032FadeOut.severity=error
dotnet_diagnostic.RCS1047FadeOut.severity=error
dotnet_diagnostic.RCS1021FadeOut.severity=error
dotnet_diagnostic.RCS1016FadeOut.severity=error
dotnet_diagnostic.RCS1049FadeOut.severity=error
dotnet_diagnostic.RCS1058FadeOut.severity=error
dotnet_diagnostic.RCS1061FadeOut.severity=error
dotnet_diagnostic.RCS1066FadeOut.severity=error
dotnet_diagnostic.RCS1085FadeOut.severity=error
dotnet_diagnostic.RCS1089FadeOut.severity=error
dotnet_diagnostic.RCS1091FadeOut.severity=error
dotnet_diagnostic.RCS1112FadeOut.severity=error
dotnet_diagnostic.RCS1114FadeOut.severity=error
dotnet_diagnostic.RCS1118.severity=error
dotnet_diagnostic.RCS1181.severity=error
dotnet_diagnostic.RCS1243.severity=error
dotnet_diagnostic.RCS1015.severity=error
dotnet_diagnostic.RCS1044.severity=error
dotnet_diagnostic.RCS1060.severity=error
dotnet_diagnostic.RCS1138.severity=error
dotnet_diagnostic.RCS1140.severity=error
dotnet_diagnostic.RCS1141.severity=error
dotnet_diagnostic.RCS1142.severity=error
dotnet_diagnostic.RCS1168.severity=error
dotnet_diagnostic.CA1507.severity=error
dotnet_diagnostic.RCS1046.severity=error
dotnet_diagnostic.RCS1047.severity=error
dotnet_diagnostic.MVC1004.severity=error
dotnet_diagnostic.RCS1077.severity=error
dotnet_diagnostic.RCS1080.severity=error
dotnet_diagnostic.RCS1096.severity=error
dotnet_diagnostic.RCS1197.severity=error
dotnet_diagnostic.RCS1198.severity=error
dotnet_diagnostic.RCS1231.severity=error
dotnet_diagnostic.RCS1235.severity=error
dotnet_diagnostic.RCS1242.severity=error
dotnet_diagnostic.CA1820.severity=error
dotnet_diagnostic.CA1825.severity=error
dotnet_diagnostic.RCS1019.severity=error
dotnet_diagnostic.RCS1052.severity=error
dotnet_diagnostic.RCS1056.severity=none
dotnet_diagnostic.RCS1081.severity=error
dotnet_diagnostic.RCS1014i.severity=error
dotnet_diagnostic.RCS1014a.severity=error
dotnet_diagnostic.RemoveUnnecessaryImportsFixable.severity=error
dotnet_diagnostic.RCS1016a.severity=error
dotnet_diagnostic.RCS1016b.severity=error
dotnet_diagnostic.RCS1036a.severity=error
dotnet_diagnostic.RCS1045a.severity=error
dotnet_diagnostic.RCS1050i.severity=error
dotnet_diagnostic.RCS1051a.severity=error
dotnet_diagnostic.IDE0064.severity=error
dotnet_diagnostic.IDE0076.severity=error
dotnet_diagnostic.IDE0077.severity=error
dotnet_diagnostic.IDE0043.severity=error
dotnet_diagnostic.RCS1059.severity=error
dotnet_diagnostic.RCS1075.severity=error
dotnet_diagnostic.RCS1102.severity=error
csharp_style_expression_bodied_lambdas=true:error
csharp_style_expression_bodied_local_functions=false:error
csharp_prefer_simple_using_statement=true:error
csharp_style_prefer_index_operator=true:error
csharp_style_prefer_range_operator=true:error
csharp_style_unused_value_assignment_preference=discard_variable:error
csharp_style_unused_value_expression_statement_preference=discard_variable:none
csharp_prefer_static_local_function=true:error

# CA1848: Use the LoggerMessage delegates
dotnet_diagnostic.CA1848.severity = none

# CA2234: Pass system uri objects instead of strings
dotnet_diagnostic.CA2234.severity = none