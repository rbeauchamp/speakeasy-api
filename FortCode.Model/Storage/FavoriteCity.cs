﻿using System.Collections.Generic;

namespace FortCode.Model.Storage
{
    /// <summary>
    /// M:N storage model for capturing a user's favorite city.
    /// </summary>
    public class FavoriteCity : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FavoriteCity"/> class.
        /// </summary>
        /// <param name="id">The entity's primary key.</param>
        public FavoriteCity(int? id) : base(id)
        {
        }

        /// <summary>
        /// The user account identifier.
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        public UserAccount? User { get; set; }

        /// <summary>
        /// Gets the city identifier.
        /// </summary>
        public int? CityId { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public City? City { get; set; }

        /// <summary>
        /// The speakeasies in this favorite city.
        /// </summary>
        public ICollection<Speakeasy> Speakeasies { get; } = new HashSet<Speakeasy>();
    }
}
