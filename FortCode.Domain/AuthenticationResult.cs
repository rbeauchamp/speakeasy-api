﻿namespace FortCode.Domain
{
    /// <summary>
    /// The result of a successful authentication.
    /// </summary>
    public class AuthenticationResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationResult"/> class.
        /// </summary>
        /// <param name="jwtSecurityToken">The JWT security token.</param>
        public AuthenticationResult(string jwtSecurityToken)
        {
            this.JwtSecurityToken = jwtSecurityToken;
        }

        /// <summary>
        /// Gets or sets the JWT security token.
        /// </summary>
        /// <value>The JWT security token.</value>
        public string JwtSecurityToken { get; }
    }
}
