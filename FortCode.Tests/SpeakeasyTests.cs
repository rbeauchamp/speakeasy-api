﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using FluentAssertions;
using FortCode.Domain;
using FortCode.Model.Api;
using FortCode.Repository;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Xunit;

namespace FortCode.Tests
{
    [Collection("SetupCollection")]
    public class SpeakeasyTests : TestsBase
    {
        public SpeakeasyTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task it_enables_an_authenticated_user_to_add_a_speakeasy_for_a_favorite_city()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await this.GetJwtAsync());
            var favoriteCity = await AddFavoriteCityAsync(client);
            var speakeasy = new AddSpeakeasy(favoriteCity.Id, "Bar name", "Favorite Drink");

            // Act
            var response = await client.PostAsJsonAsync(Routes.Speakeasies, speakeasy);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            using var serviceScope = this.Factory.Services.CreateScope();
            var repository = serviceScope.ServiceProvider.GetRequiredService<IFortCodeRepository>();
            repository.Speakeasies.Should().HaveCount(1);
            repository.Speakeasies.Single().Should().BeEquivalentTo(speakeasy, options => options.ExcludingMissingMembers());
        }

        [Fact]
        public async Task it_enables_an_authenticated_user_to_list_speakeasies_for_a_favorite_city()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await this.GetJwtAsync());
            var favoriteCity = await AddFavoriteCityAsync(client);
            var addSpeakeasy = new AddSpeakeasy(favoriteCity.Id, "Bar name", "Favorite Drink");
            await client.PostAsJsonAsync(Routes.Speakeasies, addSpeakeasy);

            // Act
            var response = await client.GetAsync($"{Routes.Speakeasies}?favoriteCityId={favoriteCity.Id}");

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            var speakeasies = await GetFromJson<List<SpeakeasyData>>(response);
            speakeasies.Should().HaveCount(1);
            speakeasies.Single().Should().BeEquivalentTo(addSpeakeasy, options => options.ExcludingMissingMembers());
        }

        [Fact]
        public async Task it_enables_an_authenticated_user_to_remove_a_speakeasy()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await this.GetJwtAsync());
            var favoriteCity = await AddFavoriteCityAsync(client);
            var addSpeakeasy = new AddSpeakeasy(favoriteCity.Id, "Bar name", "Favorite Drink");
            await client.PostAsJsonAsync(Routes.Speakeasies, addSpeakeasy);
            var response = await client.GetAsync($"{Routes.Speakeasies}?favoriteCityId={favoriteCity.Id}");
            var speakeasy = (await GetFromJson<List<SpeakeasyData>>(response)).Single();

            // Act
            var responseTwo = await client.DeleteAsync(Routes.SpeakeasyById.Replace("{id}", speakeasy.Id.ToString()));

            // Assert
            responseTwo.IsSuccessStatusCode.Should().BeTrue();
            var responseThree = await client.GetAsync($"{Routes.Speakeasies}?favoriteCityId={favoriteCity.Id}");
            var speakeasies = await GetFromJson<List<SpeakeasyData>>(responseThree);
            speakeasies.Should().HaveCount(0);
        }

        [Fact]
        public async Task it_enables_an_authenticated_user_to_list_shared_speakeasies()
        {
            // Arrange
            var userOne = await this.AddSpeakeasy("userOne@test.com");
            await this.AddSpeakeasy("userTwo@test.com");
            await this.AddSpeakeasy("userThree@test.com");

            // Act
            var response = await userOne.GetAsync(Routes.SharedSpeakeasies);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            var sharedSpeakeasies = await GetFromJson<List<SharedSpeakeasyView>>(response);
            sharedSpeakeasies.Should().HaveCount(1);
            sharedSpeakeasies.Single().BarName.Should().Be("The Chapter Room");
            sharedSpeakeasies.Single().SharedCount.Should().Be(2);
        }

        private async Task<HttpClient> AddSpeakeasy(string emailAddress = "test@test.com")
        {
            var client = this.Factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await this.GetJwtAsync(emailAddress));
            var favoriteCity = await AddFavoriteCityAsync(client);
            var addSpeakeasy = new AddSpeakeasy(favoriteCity.Id, "The Chapter Room", "Martini");
            await client.PostAsJsonAsync(Routes.Speakeasies, addSpeakeasy);
            return client;
        }

        private static async Task<FavoriteCityData> AddFavoriteCityAsync(HttpClient client)
        {
            var addFavoriteCity = new AddFavoriteCity("San Luis Obispo", "US");
            await client.PostAsJsonAsync(Routes.FavoriteCities, addFavoriteCity);
            var response = await client.GetAsync(Routes.FavoriteCities);
            return (await GetFromJson<List<FavoriteCityData>>(response)).Single();
        }

        private static async Task<T> GetFromJson<T>(HttpResponseMessage response)
        {
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        private async Task<string> GetJwtAsync(string emailAddress = "test@test.com")
        {
            // Arrange
            var client = this.Factory.CreateClient();
            var createUserAccount = new CreateUserAccount("Martha Jones", emailAddress, "M307q$cb5xp21#!S");
            await client.PostAsJsonAsync(Routes.UserAccounts, createUserAccount);
            var authenticate = new Authenticate(createUserAccount.Email, createUserAccount.Password);

            // Act
            var response = await client.PostAsJsonAsync(Routes.Authenticate, authenticate);
            var authResultJson = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<AuthenticationResult>(authResultJson).JwtSecurityToken;
        }
    }
}
