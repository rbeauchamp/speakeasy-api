<!-- Generator: Widdershins v4.0.1 -->

<h1 id="speakeasy-api">Speakeasy API v1</h1>

> Scroll down for code samples, example requests and responses. Select a language for code samples from the tabs above or the mobile navigation menu.

Enables users to manage a list of their favorite speakeasies and drinks.

Email: <a href="mailto:rbeauchamp@gmail.com">Richard Beauchamp</a> 

# Authentication

* API Key (oauth2)
    - Parameter Name: **Authorization**, in: header. Standard Authorization header using the Bearer scheme. Example: "bearer {token}"

<h1 id="speakeasy-api-speakeasy">Speakeasy</h1>

## post__api_speakeasies

> Code samples

```shell
# You can also use wget
curl -X POST /api/speakeasies \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -H 'Authorization: API_KEY'

```

```http
POST /api/speakeasies HTTP/1.1

Content-Type: application/json
Accept: application/json

```

`POST /api/speakeasies`

*Add a speakeasy in the authenticated user's favorite city.*

> Body parameter

```json
{
  "favoriteCityId": 1,
  "barName": "The Chapter Room",
  "favoriteDrinkName": "Martini"
}
```

<h3 id="post__api_speakeasies-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[AddSpeakeasy](#schemaaddspeakeasy)|true|The speakeasy data.|

> Example responses

> 400 Response

```json
{
  "property1": [
    "string"
  ],
  "property2": [
    "string"
  ]
}
```

<h3 id="post__api_speakeasies-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|The speakeasy was successfully added.|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|The speakeasy data is invalid.|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|The user is not authenticated.|[ProblemDetails](#schemaproblemdetails)|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|

<h3 id="post__api_speakeasies-responseschema">Response Schema</h3>

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» **additionalProperties**|[string]|false|none|none|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
oauth2
</aside>

## get__api_speakeasies

> Code samples

```shell
# You can also use wget
curl -X GET /api/speakeasies \
  -H 'Accept: application/json' \
  -H 'Authorization: API_KEY'

```

```http
GET /api/speakeasies HTTP/1.1

Accept: application/json

```

`GET /api/speakeasies`

*Retrieves a list of the currently authenticated user's speakeasies for the given favorite city.*

<h3 id="get__api_speakeasies-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|favoriteCityId|query|integer(int32)|false|none|

> Example responses

> 200 Response

```json
[
  {
    "id": 1,
    "city": {
      "id": 1,
      "cityName": "San Luis Obispo",
      "country": "US"
    },
    "barName": "The Chapter Room",
    "favoriteDrinkName": "Martini"
  }
]
```

<h3 id="get__api_speakeasies-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|The user's speakeasies were retrieved.|Inline|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|The favorite city id is invalid.|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|The user is not authenticated.|[ProblemDetails](#schemaproblemdetails)|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|

<h3 id="get__api_speakeasies-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[SpeakeasyData](#schemaspeakeasydata)]|false|none|[Public model for a speakeasy.]|
|» id|integer(int32)|false|none|The primary key|
|» city|[FavoriteCityData](#schemafavoritecitydata)|true|none|Public model for a user's favorite city.|
|»» id|integer(int32)|false|none|The primary key|
|»» cityName|string|true|none|The name of the city.|
|»» country|string|true|none|The 2 letter code of the country.|
|» barName|string|true|none|The name of the speakeasy.|
|» favoriteDrinkName|string|true|none|The name of the favorite drink.|

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» **additionalProperties**|[string]|false|none|none|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
oauth2
</aside>

## post__api_favoriteCities

> Code samples

```shell
# You can also use wget
curl -X POST /api/favoriteCities \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -H 'Authorization: API_KEY'

```

```http
POST /api/favoriteCities HTTP/1.1

Content-Type: application/json
Accept: application/json

```

`POST /api/favoriteCities`

*Add a favorite city for an authenticated user if not already added.*

> Body parameter

```json
{
  "cityName": "San Luis Obispo",
  "country": "US"
}
```

<h3 id="post__api_favoritecities-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[AddFavoriteCity](#schemaaddfavoritecity)|true|The favorite city data.|

> Example responses

> 400 Response

```json
{
  "property1": [
    "string"
  ],
  "property2": [
    "string"
  ]
}
```

<h3 id="post__api_favoritecities-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|202|[Accepted](https://tools.ietf.org/html/rfc7231#section-6.3.3)|The favorite city was successfully added.|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|The favorite city data is invalid.|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|The user is not authenticated.|[ProblemDetails](#schemaproblemdetails)|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|

<h3 id="post__api_favoritecities-responseschema">Response Schema</h3>

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» **additionalProperties**|[string]|false|none|none|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
oauth2
</aside>

## get__api_favoriteCities

> Code samples

```shell
# You can also use wget
curl -X GET /api/favoriteCities \
  -H 'Accept: application/json' \
  -H 'Authorization: API_KEY'

```

```http
GET /api/favoriteCities HTTP/1.1

Accept: application/json

```

`GET /api/favoriteCities`

*Retrieves a list of the currently authenticated user's favorite cities.*

> Example responses

> 200 Response

```json
[
  {
    "id": 1,
    "cityName": "San Luis Obispo",
    "country": "US"
  }
]
```

<h3 id="get__api_favoritecities-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|The user's favorite cities were retrieved.|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|The user is not authenticated.|[ProblemDetails](#schemaproblemdetails)|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|

<h3 id="get__api_favoritecities-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[FavoriteCityData](#schemafavoritecitydata)]|false|none|[Public model for a user's favorite city.]|
|» id|integer(int32)|false|none|The primary key|
|» cityName|string|true|none|The name of the city.|
|» country|string|true|none|The 2 letter code of the country.|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
oauth2
</aside>

## get__api_sharedSpeakeasies

> Code samples

```shell
# You can also use wget
curl -X GET /api/sharedSpeakeasies \
  -H 'Accept: application/json' \
  -H 'Authorization: API_KEY'

```

```http
GET /api/sharedSpeakeasies HTTP/1.1

Accept: application/json

```

`GET /api/sharedSpeakeasies`

*Retrieves a list of the shared speakeasies for the given user.*

> Example responses

> 200 Response

```json
[
  {
    "barName": "The Chapter Room",
    "sharedCount": 3
  }
]
```

<h3 id="get__api_sharedspeakeasies-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|The shared speakeasies were retrieved.|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|The user is not authenticated.|[ProblemDetails](#schemaproblemdetails)|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|

<h3 id="get__api_sharedspeakeasies-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[SharedSpeakeasyView](#schemasharedspeakeasyview)]|false|none|[Public model for a shared speakeasy.]|
|» barName|string|true|none|The name of the speakeasy.|
|» sharedCount|integer(int32)|false|none|The number of other users that share it.|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
oauth2
</aside>

## delete__api_favoriteCities_{id}

> Code samples

```shell
# You can also use wget
curl -X DELETE /api/favoriteCities/{id} \
  -H 'Accept: application/json' \
  -H 'Authorization: API_KEY'

```

```http
DELETE /api/favoriteCities/{id} HTTP/1.1

Accept: application/json

```

`DELETE /api/favoriteCities/{id}`

*Deletes a favorite city if it exists for the authenticated user.*

<h3 id="delete__api_favoritecities_{id}-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int32)|true|The id of the favorite city to delete.|

> Example responses

> 200 Response

```json
[
  {
    "id": 1,
    "cityName": "San Luis Obispo",
    "country": "US"
  }
]
```

<h3 id="delete__api_favoritecities_{id}-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|The user's favorite cities was deleted if it existed.|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|The user is not authenticated.|[ProblemDetails](#schemaproblemdetails)|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|

<h3 id="delete__api_favoritecities_{id}-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[FavoriteCityData](#schemafavoritecitydata)]|false|none|[Public model for a user's favorite city.]|
|» id|integer(int32)|false|none|The primary key|
|» cityName|string|true|none|The name of the city.|
|» country|string|true|none|The 2 letter code of the country.|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
oauth2
</aside>

## delete__api_speakeasies_{id}

> Code samples

```shell
# You can also use wget
curl -X DELETE /api/speakeasies/{id} \
  -H 'Accept: application/json' \
  -H 'Authorization: API_KEY'

```

```http
DELETE /api/speakeasies/{id} HTTP/1.1

Accept: application/json

```

`DELETE /api/speakeasies/{id}`

*Deletes a speakeasy if it exists for the authenticated user.*

<h3 id="delete__api_speakeasies_{id}-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int32)|true|The id of the speakeasy to delete.|

> Example responses

> 200 Response

```json
[
  {
    "id": 1,
    "cityName": "San Luis Obispo",
    "country": "US"
  }
]
```

<h3 id="delete__api_speakeasies_{id}-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|The user's speakeasy was deleted if it existed.|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|The user is not authenticated.|[ProblemDetails](#schemaproblemdetails)|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|

<h3 id="delete__api_speakeasies_{id}-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[FavoriteCityData](#schemafavoritecitydata)]|false|none|[Public model for a user's favorite city.]|
|» id|integer(int32)|false|none|The primary key|
|» cityName|string|true|none|The name of the city.|
|» country|string|true|none|The 2 letter code of the country.|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
oauth2
</aside>

<h1 id="speakeasy-api-useraccounts">UserAccounts</h1>

## post__api_userAccounts

> Code samples

```shell
# You can also use wget
curl -X POST /api/userAccounts \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json'

```

```http
POST /api/userAccounts HTTP/1.1

Content-Type: application/json
Accept: application/json

```

`POST /api/userAccounts`

*Create a user account.*

> Body parameter

```json
{
  "name": "Martha Jones",
  "email": "test@test.com",
  "password": "dLF!oO10C$co$19q"
}
```

<h3 id="post__api_useraccounts-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[CreateUserAccount](#schemacreateuseraccount)|true|A user's account data.|

> Example responses

> 400 Response

```json
{
  "property1": [
    "string"
  ],
  "property2": [
    "string"
  ]
}
```

<h3 id="post__api_useraccounts-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|The user account was successfully created.|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|The user account data is invalid.|Inline|

<h3 id="post__api_useraccounts-responseschema">Response Schema</h3>

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» **additionalProperties**|[string]|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

## post__api_authenticate

> Code samples

```shell
# You can also use wget
curl -X POST /api/authenticate \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json'

```

```http
POST /api/authenticate HTTP/1.1

Content-Type: application/json
Accept: application/json

```

`POST /api/authenticate`

*Authenticate a user.*

> Body parameter

```json
{
  "email": "test@test.com",
  "password": "dLF!oO10C$co$19q"
}
```

<h3 id="post__api_authenticate-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[Authenticate](#schemaauthenticate)|true|The authentication data.|

> Example responses

> 200 Response

```json
{
  "jwtSecurityToken": "string"
}
```

<h3 id="post__api_authenticate-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|Successfully authenticated.|[AuthenticationResult](#schemaauthenticationresult)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|The email or password is incorrect.|Inline|

<h3 id="post__api_authenticate-responseschema">Response Schema</h3>

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» **additionalProperties**|[string]|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

# Schemas

<h2 id="tocS_AddFavoriteCity">AddFavoriteCity</h2>
<!-- backwards compatibility -->
<a id="schemaaddfavoritecity"></a>
<a id="schema_AddFavoriteCity"></a>
<a id="tocSaddfavoritecity"></a>
<a id="tocsaddfavoritecity"></a>

```json
{
  "cityName": "San Luis Obispo",
  "country": "US"
}

```

Public model for adding a favorite city.

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|cityName|string|true|none|The name of the city.|
|country|string|true|none|The 2 letter code of the country.|

<h2 id="tocS_AddSpeakeasy">AddSpeakeasy</h2>
<!-- backwards compatibility -->
<a id="schemaaddspeakeasy"></a>
<a id="schema_AddSpeakeasy"></a>
<a id="tocSaddspeakeasy"></a>
<a id="tocsaddspeakeasy"></a>

```json
{
  "favoriteCityId": 1,
  "barName": "The Chapter Room",
  "favoriteDrinkName": "Martini"
}

```

Public model for adding a speakeasy.

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|favoriteCityId|integer(int32)|false|none|Gets the favorite city id.|
|barName|string|true|none|Gets the name of the bar.|
|favoriteDrinkName|string|true|none|Gets the name of the favorite drink.|

<h2 id="tocS_Authenticate">Authenticate</h2>
<!-- backwards compatibility -->
<a id="schemaauthenticate"></a>
<a id="schema_Authenticate"></a>
<a id="tocSauthenticate"></a>
<a id="tocsauthenticate"></a>

```json
{
  "email": "test@test.com",
  "password": "dLF!oO10C$co$19q"
}

```

Public model for authenticating a user.

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|email|string(email)|true|none|The user's email address.|
|password|string|true|none|The user's cleartext password.|

<h2 id="tocS_AuthenticationResult">AuthenticationResult</h2>
<!-- backwards compatibility -->
<a id="schemaauthenticationresult"></a>
<a id="schema_AuthenticationResult"></a>
<a id="tocSauthenticationresult"></a>
<a id="tocsauthenticationresult"></a>

```json
{
  "jwtSecurityToken": "string"
}

```

The result of a successful authentication.

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|jwtSecurityToken|string¦null|false|none|Gets or sets the JWT security token.|

<h2 id="tocS_CreateUserAccount">CreateUserAccount</h2>
<!-- backwards compatibility -->
<a id="schemacreateuseraccount"></a>
<a id="schema_CreateUserAccount"></a>
<a id="tocScreateuseraccount"></a>
<a id="tocscreateuseraccount"></a>

```json
{
  "name": "Martha Jones",
  "email": "test@test.com",
  "password": "dLF!oO10C$co$19q"
}

```

Public model for a creating a user account.

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|name|string|true|none|The user's full name.|
|email|string(email)|true|none|The user's email address.|
|password|string|true|none|The user's cleartext password.|

<h2 id="tocS_FavoriteCityData">FavoriteCityData</h2>
<!-- backwards compatibility -->
<a id="schemafavoritecitydata"></a>
<a id="schema_FavoriteCityData"></a>
<a id="tocSfavoritecitydata"></a>
<a id="tocsfavoritecitydata"></a>

```json
{
  "id": 1,
  "cityName": "San Luis Obispo",
  "country": "US"
}

```

Public model for a user's favorite city.

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int32)|false|none|The primary key|
|cityName|string|true|none|The name of the city.|
|country|string|true|none|The 2 letter code of the country.|

<h2 id="tocS_ProblemDetails">ProblemDetails</h2>
<!-- backwards compatibility -->
<a id="schemaproblemdetails"></a>
<a id="schema_ProblemDetails"></a>
<a id="tocSproblemdetails"></a>
<a id="tocsproblemdetails"></a>

```json
{
  "type": "string",
  "title": "string",
  "status": 0,
  "detail": "string",
  "instance": "string",
  "property1": null,
  "property2": null
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|**additionalProperties**|any|false|none|none|
|type|string¦null|false|none|none|
|title|string¦null|false|none|none|
|status|integer(int32)¦null|false|none|none|
|detail|string¦null|false|none|none|
|instance|string¦null|false|none|none|

<h2 id="tocS_SharedSpeakeasyView">SharedSpeakeasyView</h2>
<!-- backwards compatibility -->
<a id="schemasharedspeakeasyview"></a>
<a id="schema_SharedSpeakeasyView"></a>
<a id="tocSsharedspeakeasyview"></a>
<a id="tocssharedspeakeasyview"></a>

```json
{
  "barName": "The Chapter Room",
  "sharedCount": 3
}

```

Public model for a shared speakeasy.

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|barName|string|true|none|The name of the speakeasy.|
|sharedCount|integer(int32)|false|none|The number of other users that share it.|

<h2 id="tocS_SpeakeasyData">SpeakeasyData</h2>
<!-- backwards compatibility -->
<a id="schemaspeakeasydata"></a>
<a id="schema_SpeakeasyData"></a>
<a id="tocSspeakeasydata"></a>
<a id="tocsspeakeasydata"></a>

```json
{
  "id": 1,
  "city": {
    "id": 1,
    "cityName": "San Luis Obispo",
    "country": "US"
  },
  "barName": "The Chapter Room",
  "favoriteDrinkName": "Martini"
}

```

Public model for a speakeasy.

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int32)|false|none|The primary key|
|city|[FavoriteCityData](#schemafavoritecitydata)|true|none|Public model for a user's favorite city.|
|barName|string|true|none|The name of the speakeasy.|
|favoriteDrinkName|string|true|none|The name of the favorite drink.|

