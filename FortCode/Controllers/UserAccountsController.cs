﻿using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using FortCode.Domain;
using FortCode.Model.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace FortCode.Controllers
{
    /// <summary>
    /// Manages user accounts.
    /// Implements <see cref="ControllerBase" />
    /// </summary>
    /// <seealso cref="ControllerBase" />
    [ApiController]
    public class UserAccountsController : ControllerBase
    {
        private readonly IUserAccountsDomain userAccountsDomain;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAccountsController"/> class.
        /// </summary>
        public UserAccountsController(IUserAccountsDomain userAccountsDomain)
        {
            this.userAccountsDomain = userAccountsDomain;
        }

        /// <summary>
        ///     Create a user account.
        /// </summary>
        /// <param name="createUserAccount">A user's account data.</param>
        [AllowAnonymous]
        [HttpPost(Routes.UserAccounts)]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [SwaggerResponse(StatusCodes.Status201Created, "The user account was successfully created.")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "The user account data is invalid.", typeof(IDictionary<string, IList<string>>))]
        public async Task<IActionResult> Post([SwaggerRequestBody(Required = true)] CreateUserAccount createUserAccount)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            await this.userAccountsDomain.CreateUserAccountAsync(createUserAccount);

            return this.StatusCode(StatusCodes.Status201Created);
        }

        /// <summary>
        /// Authenticate a user.
        /// </summary>
        /// <param name="authenticate">The authentication data.</param>
        [AllowAnonymous]
        [HttpPost(Routes.Authenticate)]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully authenticated.", typeof(AuthenticationResult))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "The email or password is incorrect.", typeof(IDictionary<string, IList<string>>))]
        public async Task<IActionResult> Post([SwaggerRequestBody(Required = true)] Authenticate authenticate)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var authResult = await this.userAccountsDomain.AuthenticateAsync(authenticate);

            if (authResult == null)
            {
                this.ModelState.AddModelError(nameof(Authenticate), "The email or password is incorrect.");
                return this.BadRequest(this.ModelState);
            }

            return this.Ok(authResult);
        }
    }
}
