﻿using System.Threading.Tasks;
using FortCode.Model.Api;
using FortCode.Model.Storage;
using FortCode.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using BCryptNet = BCrypt.Net.BCrypt;

namespace FortCode.Domain
{
    /// <inheritdoc />
    public class UserAccountsDomain : IUserAccountsDomain
    {
        private readonly IFortCodeRepository repository;
        private readonly IConfiguration configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAccountsDomain" /> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="configuration">The configuration.</param>
        public UserAccountsDomain(IFortCodeRepository repository, IConfiguration configuration)
        {
            this.repository = repository;
            this.configuration = configuration;
        }

        /// <inheritdoc />
        public async Task CreateUserAccountAsync(CreateUserAccount createUserAccount)
        {
            var userAccount = Map(createUserAccount);
            await this.repository.UserAccounts.AddAsync(userAccount);
            await this.repository.SaveChangesAsync();
        }

        /// <inheritdoc />
        public Task<bool> EmailExistsAsync(string emailAddress)
        {
            return this.repository.UserAccounts.AnyAsync(user => user.Email == emailAddress);
        }

        /// <inheritdoc />
        public Task<bool> UserExistsAsync(int userId)
        {
            return this.repository.UserAccounts.AnyAsync(user => user.Id == userId);
        }

        /// <inheritdoc />
        public async Task<AuthenticationResult?> AuthenticateAsync(Authenticate authenticate)
        {
            var userAccount = await this.repository.UserAccounts.SingleOrDefaultAsync(account => account.Email == authenticate.Email);

            if (userAccount == null || !BCryptNet.EnhancedVerify(authenticate.Password, userAccount.Password))
            {
                return null;
            }

            return new AuthenticationResult(JwtGenerator.GenerateToken(userAccount, this.configuration[ConfigKeys.SigningKey]));
        }

        private static UserAccount Map(CreateUserAccount createUserAccount)
        {
            return new UserAccount(default, createUserAccount.Name, createUserAccount.Email, BCryptNet.EnhancedHashPassword(createUserAccount.Password));
        }
    }
}
