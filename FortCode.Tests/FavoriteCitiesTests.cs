﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using FluentAssertions;
using FortCode.Domain;
using FortCode.Model.Api;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Xunit;

namespace FortCode.Tests
{
    [Collection("SetupCollection")]
    public class FavoriteCitiesTests : TestsBase
    {
        public FavoriteCitiesTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task it_requires_an_authenticated_user_to_add_a_favorite_city()
        {
            // Arrange
            var client = this.Factory.CreateClient();


            // Act
            var response = await client.PostAsJsonAsync(Routes.FavoriteCities, new object());

            // Assert
            response.IsSuccessStatusCode.Should().BeFalse();
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task it_enables_an_authenticated_user_to_add_a_favorite_city()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await this.GetJwtAsync());
            var addFavoriteCity = new AddFavoriteCity("San Luis Obispo", "US");

            // Act
            var response = await client.PostAsJsonAsync(Routes.FavoriteCities, addFavoriteCity);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            response.StatusCode.Should().Be(HttpStatusCode.Accepted);
        }

        [Fact]
        public async Task it_enables_an_authenticated_user_to_remove_a_favorite_city()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await this.GetJwtAsync());
            var addFavoriteCity = new AddFavoriteCity("San Luis Obispo", "US");
            await client.PostAsJsonAsync(Routes.FavoriteCities, addFavoriteCity);
            var responseOne = await client.GetAsync(new Uri(Routes.FavoriteCities, UriKind.Relative));
            var favoriteCity = (await GetFromJson<List<FavoriteCityData>>(responseOne)).Single();

            // Act
            var responseTwo = await client.DeleteAsync(Routes.FavoriteCityById.Replace("{id}", favoriteCity.Id.ToString()));

            // Assert
            responseTwo.IsSuccessStatusCode.Should().BeTrue();
            responseTwo.StatusCode.Should().Be(HttpStatusCode.OK);
            var responseThree = await client.GetAsync(new Uri(Routes.FavoriteCities, UriKind.Relative));
            (await GetFromJson<List<FavoriteCityData>>(responseThree)).Should().BeEmpty();
        }

        [Fact]
        public async Task it_enables_an_authenticated_user_to_retrieve_a_list_of_favorite_cities()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await this.GetJwtAsync());
            var addFavoriteCity = new AddFavoriteCity("San Luis Obispo", "US");
            await client.PostAsJsonAsync(Routes.FavoriteCities, addFavoriteCity);

            // Act
            var response = await client.GetAsync(Routes.FavoriteCities);

            // Assert
            var favoriteCities = await GetFromJson<List<FavoriteCityData>>(response);
            favoriteCities.Should().HaveCount(1);
            favoriteCities.Single().Should().BeEquivalentTo(addFavoriteCity, options => options.ExcludingMissingMembers());
        }

        private static async Task<T> GetFromJson<T>(HttpResponseMessage response)
        {
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        private async Task<string> GetJwtAsync()
        {
            // Arrange
            var client = this.Factory.CreateClient();
            var createUserAccount = new CreateUserAccount("Martha Jones", "test@test.com", "M307q$cb5xp21#!S");
            await client.PostAsJsonAsync(Routes.UserAccounts, createUserAccount);
            var authenticate = new Authenticate(createUserAccount.Email, createUserAccount.Password);

            // Act
            var response = await client.PostAsJsonAsync(Routes.Authenticate, authenticate);
            var authResultJson = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<AuthenticationResult>(authResultJson).JwtSecurityToken;
        }
    }
}
