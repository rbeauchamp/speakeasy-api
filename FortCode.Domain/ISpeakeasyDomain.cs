﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Model.Api;

namespace FortCode.Domain
{
    /// <summary>
    /// Business domain for managing speakeasies.
    /// </summary>
    public interface ISpeakeasyDomain
    {
        /// <summary>
        /// Adds a favorite city for the given user if it doesn't exist yet.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="addFavoriteCity">The favorite city to add.</param>
        Task AddFavoriteCityAsync(int userId, AddFavoriteCity addFavoriteCity);

        /// <summary>
        /// Retrieves a list of the given user's favorite cities.
        /// </summary>
        /// <param name="userId">The user id.</param>
        Task<List<FavoriteCityData>> GetFavoriteCitiesAsync(int userId);

        /// <summary>
        /// Deletes the favorite city with the given id.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="favoriteCityId">The favorite city id.</param>
        Task DeleteFavoriteCityAsync(int userId, int favoriteCityId);

        /// <summary>
        /// Adds a speakeasy for the given user if it doesn't exist yet.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="addSpeakeasy">The speakeasy to add.</param>
        Task AddSpeakeasyAsync(int userId, AddSpeakeasy addSpeakeasy);

        /// <summary>
        /// Determines whether the given favoriteCityId is for the given user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="favoriteCityId">The favorite city identifier.</param>
        Task<bool> IsValidCityIdForUserAsync(int userId, int favoriteCityId);

        /// <summary>
        /// Gets the speakeasies assoicated with the given favorite city
        /// </summary>
        /// <param name="favoriteCityId">The favorite city identifier.</param>
        /// <returns>Task&lt;List&lt;SpeakeasyData&gt;&gt;.</returns>
        Task<List<SpeakeasyData>> GetSpeakeasiesAsync(int favoriteCityId);

        /// <summary>
        /// Deletes the speakeasy with the given id.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="speakeasyId">The speakeasy id.</param>
        Task DeleteSpeakeasyAsync(int userId, int speakeasyId);

        /// <summary>
        /// Gets the shared speakeasies for the given user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        Task<List<SharedSpeakeasyView>> GetSharedSpeakeasiesAsync(int userId);
    }
}
