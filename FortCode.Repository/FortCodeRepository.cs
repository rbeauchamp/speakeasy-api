﻿using FortCode.Model.Storage;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Repository
{
    /// <summary>
    /// The repository for the FortCodeDb.
    /// </summary>
    public class FortCodeRepository : DbContext, IFortCodeRepository
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FortCodeRepository" /> class.
        /// </summary>
        /// <param name="options">The db options.</param>
        public FortCodeRepository(DbContextOptions<FortCodeRepository> options) : base(options)
        {
        }

        /// <inheritdoc />
        public DbSet<UserAccount> UserAccounts => this.Set<UserAccount>();

        /// <inheritdoc />
        public DbSet<City> Cities => this.Set<City>();

        /// <inheritdoc />
        public DbSet<FavoriteCity> FavoriteCities => this.Set<FavoriteCity>();

        /// <inheritdoc />
        public DbSet<Speakeasy> Speakeasies => this.Set<Speakeasy>();

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserAccount>(
                entity =>
                {
                    entity.ToTable("UserAccounts");
                    entity.HasKey(e => e.Id);
                    entity.Property(e => e.Id).ValueGeneratedOnAdd();
                    entity.Property(e => e.Name).IsRequired();
                    entity.Property(e => e.Email).IsRequired();
                    entity.Property(e => e.Password).IsRequired();
                    entity.HasIndex(e => e.Email).IsUnique();
                });

            modelBuilder.Entity<City>(
                entity =>
                {
                    entity.ToTable("Cities");
                    entity.HasKey(e => e.Id);
                    entity.Property(e => e.Id).ValueGeneratedOnAdd();
                    entity.Property(e => e.CityName).IsRequired();
                    entity.Property(e => e.Country).IsRequired();
                    entity.HasIndex(e => new { e.CityName, e.Country }).IsUnique();
                });

            modelBuilder.Entity<FavoriteCity>(
                entity =>
                {
                    entity.ToTable("FavoriteCities");
                    entity.HasKey(e => e.Id);
                    entity.Property(e => e.Id).ValueGeneratedOnAdd();
                    entity.HasOne(e => e.User)
                        .WithMany(e => e!.FavoriteCities)
                        .HasForeignKey(e => e.UserId);
                    entity.HasOne(e => e.City)
                        .WithMany(e => e!.FavoriteCities)
                        .HasForeignKey(e => e.CityId);
                    entity.HasIndex(e => new { e.UserId, e.CityId }).IsUnique();
                });

            modelBuilder.Entity<Speakeasy>(
                entity =>
                {
                    entity.ToTable("Speakeasies");
                    entity.HasKey(e => e.Id);
                    entity.Property(e => e.Id).ValueGeneratedOnAdd();
                    entity.Property(e => e.BarName);
                    entity.Property(e => e.FavoriteDrinkName);
                    entity.HasOne(e => e.FavoriteCity)
                        .WithMany(e => e!.Speakeasies)
                        .HasForeignKey(e => e.FavoriteCityId);
                    entity.HasIndex(e => new { e.FavoriteCityId, e.BarName }).IsUnique();
                });
        }
    }
}
