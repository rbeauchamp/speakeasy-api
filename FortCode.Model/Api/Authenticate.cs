﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Api
{
    /// <summary>
    /// Public model for authenticating a user.
    /// </summary>
    public class Authenticate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Authenticate"/> class.
        /// </summary>
        /// <param name="email">The user's email address.</param>
        /// <param name="password">The user's cleartext password.</param>
        /// <exception cref="ArgumentNullException">name</exception>
        /// <exception cref="ArgumentNullException">email</exception>
        /// <exception cref="ArgumentNullException">password</exception>
        public Authenticate(string email, string password)
        {
            this.Email = email ?? throw new ArgumentNullException(nameof(email));
            this.Password = password ?? throw new ArgumentNullException(nameof(password));
        }

        /// <summary>
        /// The user's email address.
        /// </summary>
        /// <example>test@test.com</example>
        [Required]
        [EmailAddress]
        [StringLength(320)]
        public string Email { get; }

        /// <summary>
        /// The user's cleartext password.
        /// </summary>
        /// <example>dLF!oO10C$co$19q</example>
        [Required]
        [StringLength(50, MinimumLength = 8)]
        public string Password { get; }
    }
}
