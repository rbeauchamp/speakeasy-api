﻿using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using FluentValidation.AspNetCore;
using FortCode.Domain;
using FortCode.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Filters;

namespace FortCode
{
    /// <summary>
    ///     Configuration for the api.
    /// </summary>
    public class Startup
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Startup" /> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        /// <summary>
        ///     Gets the configuration.
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">The services.</param>
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<CreateUserAccountValidator>());

            services
                .AddControllers()
                .AddNewtonsoftJson(
                    options =>
                    {
                        options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                        options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                        options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    });

            AddSwaggerGen(services);

            this.ConfigureAuth(services);

            services.AddHttpContextAccessor();
            services.AddLogging();

            services.AddDbContext<IFortCodeRepository, FortCodeRepository>(optionsBuilder =>
            {
                var connectionString = this.Configuration[ConfigKeys.DbConnectionString];
                optionsBuilder.UseSqlServer(connectionString);
            });

            services.AddScoped<IUserAccountsDomain, UserAccountsDomain>();
            services.AddScoped<ISpeakeasyDomain, SpeakeasyDomain>();
            services.AddSingleton(this.Configuration);
        }

        private static void AddSwaggerGen(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Speakeasy API",
                    Version = "v1",
                    Description = "Enables users to manage a list of their favorite speakeasies and drinks.",
                    Contact = new OpenApiContact
                    {
                        Name = "Richard Beauchamp",
                        Email = "rbeauchamp@gmail.com"
                    },
                });

                // add Security information to each operation for OAuth2
                options.OperationFilter<SecurityRequirementsOperationFilter>();
                // or use the generic method, e.g. c.OperationFilter<SecurityRequirementsOperationFilter<MyCustomAttribute>>();

                // if you're using the SecurityRequirementsOperationFilter, you also need to tell Swashbuckle you're using OAuth2
                options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Description = "Standard Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                //options.AddSecurityDefinition(jwtSecurityScheme.Reference.Id, jwtSecurityScheme);

                //options.AddSecurityRequirement(new OpenApiSecurityRequirement
                //{
                //    { jwtSecurityScheme, Array.Empty<string>() }
                //});

                options.EnableAnnotations();

                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "FortCode.xml"));
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "FortCode.Domain.xml"));
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "FortCode.Model.xml"));
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "FortCode.Repository.xml"));
            });
        }

        private void ConfigureAuth(IServiceCollection services)
        {
            services.AddAuthentication(authenticationOptions =>
                {
                    authenticationOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    authenticationOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(jwtBearerOptions =>
                {
                    jwtBearerOptions.Events = new JwtBearerEvents
                    {
                        OnTokenValidated = async context =>
                        {
                            // Verify that the user exists in the database
                            var accountsDomain = context.HttpContext.RequestServices.GetRequiredService<IUserAccountsDomain>();
                            var userId = int.Parse(context.Principal.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier).Value);
                            if (!await accountsDomain.UserExistsAsync(userId))
                            {
                                // return unauthorized if user no longer exists
                                context.Fail("Unauthorized");
                            }
                        }
                    };
                    jwtBearerOptions.RequireHttpsMetadata = false;
                    jwtBearerOptions.Audience = JwtGenerator.Audience;
                    jwtBearerOptions.SaveToken = true;
                    var key = Encoding.ASCII.GetBytes(this.Configuration[ConfigKeys.SigningKey]);
                    jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key)
                    };
                    var config = new OpenIdConnectConfiguration
                    {
                        Issuer = JwtGenerator.Issuer
                    };
                    jwtBearerOptions.Configuration = config;
                });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">The application builder.</param>
        /// <param name="env">The hosting env.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Speakeasy API v1");
                c.EnableDeepLinking();
            });

            app.UseFileServer();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endPoints => endPoints.MapControllers());
        }
    }
}
