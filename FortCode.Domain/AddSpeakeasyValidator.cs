﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FluentValidation;
using FortCode.Model.Api;
using FortCode.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Domain
{
    /// <summary>
    /// Validates AddSpeakeasy data.
    /// </summary>
    public class AddSpeakeasyValidator : AbstractValidator<AddSpeakeasy>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateUserAccountValidator" /> class.
        /// </summary>
        /// <param name="repository">The user accounts domain.</param>
        /// <param name="httpContextAccessor">The HTTP context accessor.</param>
        public AddSpeakeasyValidator(IFortCodeRepository repository, IHttpContextAccessor httpContextAccessor)
        {
            this.RuleFor(x => x).MustAsync(async (x, _) => await IsFavoriteCityForUserAsync(x, repository, httpContextAccessor)).WithMessage("Invalid favorite city id.");
            this.RuleFor(x => x).MustAsync(async (x, _) => !await SpeakeasyExistsAsync(x, repository, httpContextAccessor)).WithMessage("This speakeasy has already been added.");
        }

        private static async Task<bool> IsFavoriteCityForUserAsync(AddSpeakeasy addSpeakeasy, IFortCodeRepository repository, IHttpContextAccessor httpContextAccessor)
        {
            return await repository.FavoriteCities
                .AnyAsync(favoriteCity => favoriteCity.Id == addSpeakeasy.FavoriteCityId
                                       && favoriteCity.UserId == GetUserId(httpContextAccessor));
        }

        private static async Task<bool> SpeakeasyExistsAsync(AddSpeakeasy addSpeakeasy, IFortCodeRepository repository, IHttpContextAccessor httpContextAccessor)
        {
            return await repository.Speakeasies
                .AnyAsync(speakeasy => speakeasy.FavoriteCityId == addSpeakeasy.FavoriteCityId
                                    && speakeasy.BarName == addSpeakeasy.BarName
                                    && speakeasy.FavoriteCity!.UserId == GetUserId(httpContextAccessor));
        }

        private static int GetUserId(IHttpContextAccessor httpContextAccessor)
        {
            return int.Parse(httpContextAccessor.HttpContext.User.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier).Value);
        }
    }
}
