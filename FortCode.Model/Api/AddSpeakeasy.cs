﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Api
{
    /// <summary>
    /// Public model for adding a speakeasy.
    /// </summary>
    public class AddSpeakeasy
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddSpeakeasy"/> class.
        /// </summary>
        /// <param name="favoriteCityId">The favorite city identifier.</param>
        /// <param name="barName">Name of the bar.</param>
        /// <param name="favoriteDrinkName">Name of the favorite drink.</param>
        /// <exception cref="ArgumentNullException">barName</exception>
        /// <exception cref="ArgumentNullException">favoriteDrinkName</exception>
        public AddSpeakeasy(int favoriteCityId, string barName, string favoriteDrinkName)
        {
            this.FavoriteCityId = favoriteCityId;
            this.BarName = barName ?? throw new ArgumentNullException(nameof(barName));
            this.FavoriteDrinkName = favoriteDrinkName ?? throw new ArgumentNullException(nameof(favoriteDrinkName));
        }

        /// <summary>
        /// Gets the favorite city id.
        /// </summary>
        /// <example>1</example>
        public int FavoriteCityId { get; }

        /// <summary>
        /// Gets the name of the bar.
        /// </summary>
        /// <example>The Chapter Room</example>
        [Required]
        [StringLength(256, MinimumLength = 1)]
        public string BarName { get; }

        /// <summary>
        /// Gets the name of the favorite drink.
        /// </summary>
        /// <example>Martini</example>
        [Required]
        [StringLength(256, MinimumLength = 1)]
        public string FavoriteDrinkName { get; }
    }
}
