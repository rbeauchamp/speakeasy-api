﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Model.Storage
{
    /// <summary>
    /// CThe speakeasy storage model.
    /// </summary>
    /// <seealso cref="Entity" />
    public class Speakeasy : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Speakeasy"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="favoriteCityId">The favorite city identifier.</param>
        /// <param name="barName">Name of the bar.</param>
        /// <param name="favoriteDrinkName">Name of the favorite drink.</param>
        /// <exception cref="ArgumentNullException">barName</exception>
        /// <exception cref="ArgumentNullException">favoriteDrinkName</exception>
        public Speakeasy(int? id, int favoriteCityId, string barName, string favoriteDrinkName) : base(id)
        {
            this.FavoriteCityId = favoriteCityId;
            this.BarName = barName ?? throw new ArgumentNullException(nameof(barName));
            this.FavoriteDrinkName = favoriteDrinkName ?? throw new ArgumentNullException(nameof(favoriteDrinkName));
        }

        /// <summary>
        /// Gets the favorite city id.
        /// </summary>
        public int FavoriteCityId { get; }

        /// <summary>
        /// Gets or sets the favorite city.
        /// </summary>
        public FavoriteCity? FavoriteCity { get; set; }

        /// <summary>
        /// Gets the name of the bar.
        /// </summary>
        [Required]
        [StringLength(256)]
        public string BarName { get; }

        /// <summary>
        /// Gets the name of the favorite drink.
        /// </summary>
        [Required]
        [StringLength(256)]
        public string FavoriteDrinkName { get; }
    }
}
