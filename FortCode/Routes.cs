﻿namespace FortCode
{
    /// <summary>
    ///     The set of API routes.
    /// </summary>
    public static class Routes
    {
        /// <summary>
        ///     The base API path.
        /// </summary>
        private const string BaseApiPath = "api";

        /// <summary>
        /// Path to the accounts resource.
        /// </summary>
        public const string UserAccounts = BaseApiPath + "/userAccounts";

        /// <summary>
        /// Path to authenticate a user.
        /// </summary>
        public const string Authenticate = BaseApiPath + "/authenticate";

        /// <summary>
        /// Path to the favorite cities resource.
        /// </summary>
        public const string FavoriteCities = BaseApiPath + "/favoriteCities";

        /// <summary>
        /// Path to a favorite city by id.
        /// </summary>
        public const string FavoriteCityById = FavoriteCities + "/{id}";

        /// <summary>
        /// Path to the speakeasies resource.
        /// </summary>
        public const string Speakeasies = BaseApiPath + "/speakeasies";

        /// <summary>
        /// Path to the shared speakeasies view.
        /// </summary>
        public const string SharedSpeakeasies = BaseApiPath + "/sharedSpeakeasies";

        /// <summary>
        /// Path to a speakeasy by id.
        /// </summary>
        public const string SpeakeasyById = Speakeasies + "/{id}";
    }
}
