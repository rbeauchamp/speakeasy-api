﻿using FluentValidation;
using FortCode.Model.Api;

namespace FortCode.Domain
{
    /// <summary>
    /// Validates create user account data.
    /// </summary>
    public class CreateUserAccountValidator : AbstractValidator<CreateUserAccount>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CreateUserAccountValidator" /> class.
        /// </summary>
        /// <param name="userAccountsDomain">The user accounts domain.</param>
        public CreateUserAccountValidator(IUserAccountsDomain userAccountsDomain)
        {
            this.RuleFor(x => x).MustAsync(async (x, _) => !await userAccountsDomain.EmailExistsAsync(x.Email)).WithMessage("The email address is already being used.");
        }
    }
}
